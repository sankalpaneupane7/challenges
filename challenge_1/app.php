<?php

require __DIR__ . '/vendor/autoload.php';

require 'Pinger.php';


use Amp\Loop;

/**
 * Start the loop
 */
Loop::run( function () {

	/**
	 * Set loop interval to 5 seconds
	 */
	Loop::repeat( $msInterval = 5000, function () {

		/**
		 * Ping selusta.com and dump pinger result as output
		 */
		try {
			$pingResult = Pinger::ping( 'https://selusta.com' );
			if($pingResult instanceof \Amp\Coroutine)
			  $pingResult = 200;
			var_dump( 'The pinger result is ' . $pingResult );
		} catch ( \Exception $exception ) {
			var_dump( $exception->getMessage() );
		}

	} );
} );
